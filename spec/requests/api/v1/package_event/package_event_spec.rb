# frozen_string_literal: true
        
# When modifying this document, run the following command to generate the swagger.yaml file:
# bundle exec rake rswag:specs:swaggerize

require "swagger_helper"

RSpec.describe("api/v1/package_events") do
  # add API paths here
  path "/api/v1/package_events" do
    # index
    get("list package_events") do
      tags "Package Events"
      consumes "application/json"
      produces "application/json"
      description "List all package events in the system (index)"

      response(200, "successful") do # response code and message
        # schema defined in swagger_helper.rb
        schema type: :array, items: { "$ref" => "#/components/schemas/package_event" }

        # let(:package_event) { PackageEvent.create(name: "...") } # insert attributes

        # Examples can be automatically generated from the schema or explicitly defined
        # Uncomment one of the following 2 blocks

        # # Explicit example definition
        # example "application/json",
        #   :example_key,
        #   [
        #     {
        #       id: "...",
        #       name: "...",
        #     },
        #   ],
        #   "Single PackageEvent",
        #   "..." # Example description, optional
        # # can define multiple examples, selectable in the UI

        # Automatic example generation
        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json" => {
              examples: {
                test_example: {
                  value: JSON.parse(response.body, symbolize_names: true),
                },
              },
            },
          }
          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end
        run_test!
      end
    end
  end

  path "/api/v1/package_events/{id}" do
  parameter name: "id", in: :path, type: :integer, description: "The ID for the Package Event"
    # show
    get("show a package_event") do
      tags "Package Events"
      consumes "application/json"
      produces "application/json"
      description "Get the details for a particular package event (show)"

      response(200, "successful") do
        # schema defined in swagger_helper.rb
        schema "$ref" => "#/components/schemas/package_event"

        # let(:package_event) { PackageEvent.create(name: "...") } # insert attributes
        # let(:id) { package_event.id } # insert object name

        # Examples can be automatically generated from the schema or explicitly defined
        # Uncomment one of the following 2 blocks

        # # Explicit example definition
        # example "application/json",
        #   :example_key,
        #   [
        #     {
        #       id: "...", # insert example attributes
        #       name: "...", # insert example attributes
        #     },
        #   ],
        #   "Single PackageEvent",
        #   "Longer description of the example ..." # Example description, optional
        # # can define multiple examples, selectable in the UI

        # Automatic example generation
        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json" => {
              examples: {
                test_example: {
                  value: JSON.parse(response.body, symbolize_names: true),
                },
              },
            },
          }
          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end
        run_test!
      end

      response(404, "not found") do
        # schema defined in swagger_helper.rb
        schema type: "object",
          properties: {
            message: { type: :string },
          }

        let(:id) { 999999999 }

        example "application/json",
          :example_key,
          { "error": "package_event not found, check the id." },
          "Single PackageEvent"

        run_test!
      end
    end
  end

end
