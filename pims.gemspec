Gem::Specification.new do |s|
    s.name        = "pims"
    s.version     = "0.0.3"
    s.executables << "pims"
    s.summary     = "Pims!"
    s.add_dependency 'bundler'
    s.add_dependency 'colorize', "~> 1.1.0"
    s.add_dependency 'erb', "~> 2.2.3"
    # s.add_dependency 'rails'
    s.description = "A gem for automatically generating Swagger Docs for your API Requests."
    s.authors     = ["Cosmic"]
    s.email       = "joao.costa@theloop.com"
    s.files       = ["lib/pims.rb"] + Dir["lib/pims/**.rb"]
    s.homepage    =
      "https://rubygems.org/gems/pims"
    s.license       = "MIT"
  end