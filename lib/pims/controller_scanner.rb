class Pims::ControllerScanner
  # sets default directory path if none is given
  def parse(directory)
    @dir = directory
    if @dir.nil?
      @dir = './app/controllers/api/v1'
      puts "\nUsing default controller_path => #{@dir}\n"
    end
  end

  # gets the controller files from the directory
  def get_files
    @files_array = Dir["#{@dir}/*.rb"]

    @files_hash = {}
    # convert into hash with fixed index for each file
    @files_array.each_with_index do |path, index|
      @files_hash[index] = path
    end
  end

  # asks the user for files to remove
  def query_remove_files
    while not(@files_hash.empty?) do
      # sanitize hash output
      str = String.new
      @files_hash.each do |key,value|
        str += "(#{key}) #{value}   "
      end
      puts str.green

      # get user input
      print "\nRemove file (enter the index, empty to skip): "
      answer = STDIN.gets.chomp
      break if answer == ""

      # handle non-integer inputs
      begin
        answer = Integer(answer)
      rescue ArgumentError
        puts "Not a valid integer. Try again.\n".red
        next
      end
      
      # perform deletion and handle invalid integers
      if @files_hash.keys.include?(answer)
        puts answer
        @files_hash.delete(answer)
      else
        puts "Invalid index. Try again.\n".red
      end
    end # while
    return @files_hash
  end

  def ask_queue
    print "\nQueue files for generate spec?(y/[n]): "
    answer = STDIN.gets.chomp
    accepted = ['yes','y']

    return accepted.include?(answer.downcase)
  end
  
end