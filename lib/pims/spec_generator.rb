require 'rails/generators'
require 'erb'
require 'fileutils'

class Pims::SpecGenerator
  def initialize(file)
    @file = file    # ./spec/requests/controllers/api/v1/something_controller.rb
    @dir_name = "api/v1"
  end

  def create_spec_file
    @object_name = File.basename(@file).delete_suffix!('_controller.rb')  # package_event
    @object_name_camels = @object_name.split("_").collect(&:capitalize).join(" ")   # Package Events
    
    dir_path = "./spec/requests/" + @dir_name
    # @spec_path = dir_path + @object_name + "_spec.rb"

    @spec_path = File.join(dir_path, @object_name, @object_name + "_spec.rb")   
    #  ./spec/requests/api/package_event/package_event_spec.rb
    
    @describe_path = File.join(@dir_name, @object_name.pluralize)
    @message = ERB.new(template('bare'), trim_mode: "%<>").result(binding)
  end

  def set_manifest
    document = File.read(@file)
    @manifest = []
    actions = ['index','create','show','update']
    actions.each do |action|
      @manifest.append(action) if document.include?("def " + action)
    end
  end

  def build
    @manifest.each do |action|
      new_message = ERB.new(template(action), trim_mode: "%<>").result(binding)
      # @message = @message.gsub(/(.*)(\nend\b.*)/m, "\\1\n\n#{new_message}\\2")
      r = /
          .*
          \K
          (?=\n\s*end\b)
          
          /mx
      @message = @message.sub(r, new_message)
    end
  end

  def save
    FileUtils.mkdir_p(File.dirname(@spec_path))
    File.write(@spec_path, @message)
  end

  def template(type='bare')
    case type
    when "bare"
      return %q{# frozen_string_literal: true
        
# When modifying this document, run the following command to generate the swagger.yaml file:
# bundle exec rake rswag:specs:swaggerize

require "swagger_helper"

RSpec.describe("<%= @describe_path %>") do
  # add API paths here
end
}
    when "index"
      return %q{
  path "/<%= @dir_name %>/<%= @object_name.pluralize %>" do
    # index
    get("list <%= @object_name.pluralize %>") do
      tags "<%= @object_name_camels.pluralize %>"
      consumes "application/json"
      produces "application/json"
      description "List all <%= @object_name.pluralize.tr('_', ' ')  %> in the system (index)"

      response(200, "successful") do # rubocop: disable RSpec/NestedGroups
        # schema defined in swagger_helper.rb
        schema type: :array, items: { "$ref" => "#/components/schemas/<%= @object_name.singularize %>" }

        # let(:<%= @object_name.singularize %>) { <%= @object_name.singularize.camelcase %>.create(name: "...") } # insert attributes

        # Examples can be automatically generated from the schema or explicitly defined
        # Uncomment one of the following 2 blocks

        # # Explicit example definition
        # example "application/json",
        #   :example_key,
        #   [
        #     {
        #       id: "...",
        #       name: "...",
        #     },
        #   ],
        #   "Single <%= @object_name.singularize.camelcase %>",
        #   "..." # Example description, optional
        # # can define multiple examples, selectable in the UI

        # Automatic example generation
        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json" => {
              examples: {
                test_example: {
                  value: JSON.parse(response.body, symbolize_names: true),
                },
              },
            },
          }
          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end
        run_test!
      end
    end
  end
}
    when "create"
      return %q{
  path "/<%= @dir_name %>/<%= @object_name.pluralize %>" do
    # create
    post("create a <%= @object_name.singularize %>") do
      tags "<%= @object_name_camels.pluralize %>"
      consumes "application/json"
      produces "application/json"
      description "Create a <%= @object_name.singularize.tr('_', ' ') %> (create)"

      parameter name: :<%= @object_name.singularize %>, in: :body, schema: { "$ref" => "#/components/schemas/<%= @object_name.singularize %>" }

      response(200, "successful") do # rubocop: disable RSpec/NestedGroups
        schema "$ref" => "#/components/schemas/<%= @object_name.singularize %>"

        # let(:<%= @object_name.singularize %>) do # insert attributes
        #   {
        #     partner_id: 1,
        #     package_type_id: 1,
        #     active: true,
        #   }
        # end

        # Examples can be automatically generated from the schema or explicitly defined
        # Uncomment one of the following 2 blocks

        # # Explicit example definition
        # example "application/json",
        #  :example_key,
        #  [
        #     {
        #       id: "...",
        #       name: "...",
        #     },
        #   ],
        #   "Single <%= @object_name.singularize.camelcase %>",
        #   "..." # Example description, optional
        # # can define multiple examples, selectable in the UI

        # Automatic example generation
        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json" => {
              examples: {
                test_example: {
                  value: JSON.parse(response.body, symbolize_names: true),
                },
              },
            },
          }
          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end
}
    when "update"
      return %q{
  path "/<%= @dir_name %>/<%= @object_name.pluralize %>/{id}" do
    parameter name: "id", in: :path, type: :string, description: "The ID for the <%= @object_name_camels.singularize %>"

    # update
    patch("update <%= @object_name.pluralize %>") do
      tags "<%= @object_name_camels.pluralize %>"
      consumes "application/json"
      produces "application/json"
      description "Update a <%= @object_name.singularize.tr('_', ' ') %> (update)"

      parameter name: :<%= @object_name %>,
        in: :body,
        schema: { "$ref" => "#/components/schemas/<%= @object_name.singularize %> " }
      response(200, "successful") do # rubocop: disable RSpec/NestedGroups
        # schema defined in swagger_helper.rb
        schema "$ref" => "#/components/schemas/<%= @object_name.singularize %> "

        # let(:<%= @object_name.singularize %>) { <%= @object_name.singularize.camelcase %>.create(name: "...") } # insert attributes
        # let(:id) { <%= @object_name.singularize %>.id }
        # Examples can be automatically generated from the schema or explicitly defined
        # Uncomment one of the following 2 blocks

        # # Explicit example definition
        # example "application/json",
        #   :example_key,
        #   [
        #     {
        #       id: "...", # insert example attributes
        #       name: "...", # insert example attributes
        #     },
        #   ],
        #   "Single <%= @object_name.singularize.camelcase %>",
        #   "Longer description of the example ..." # Example description, optional
        # # can define multiple examples, selectable in the UI

        # Automatic example generation
        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json" => {
              examples: {
                test_example: {
                  value: JSON.parse(response.body, symbolize_names: true),
                },
              },
            },
          }
          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end
        run_test!
      end

      response(404, "not found") do # rubocop: disable RSpec/NestedGroups
        # schema defined in swagger_helper.rb
        schema type: "object",
          properties: {
            message: { type: :string },
          }

        let(:id) { 999999999 }

        example "application/json",
          :example_key,
          { "error": " <%= @object_name.singularize %> not found, check the id." },
          "Single <%= @object_name.singularize.camelcase %>"

        run_test!
      end
    end
  end
}
    when "show"
      return %q{
  path "/<%= @dir_name %>/<%= @object_name.pluralize %>/{id}" do
  parameter name: "id", in: :path, type: :integer, description: "The ID for the <%= @object_name_camels.singularize%>"
    # show
    get("show a <%= @object_name.singularize %>") do
      tags "<%= @object_name_camels.pluralize %>"
      consumes "application/json"
      produces "application/json"
      description "Get the details for a particular <%=@object_name.singularize.tr('_', ' ') %> (show)"

      response(200, "successful") do # rubocop: disable RSpec/NestedGroups
        # schema defined in swagger_helper.rb
        schema "$ref" => "#/components/schemas/<%= @object_name.singularize %>"

        # let(:<%= @object_name.singularize %>) { <%= @object_name.singularize.camelcase %>.create(name: "...") } # insert attributes
        # let(:id) { <%= @object_name.singularize %>.id } # insert object name

        # Examples can be automatically generated from the schema or explicitly defined
        # Uncomment one of the following 2 blocks

        # # Explicit example definition
        # example "application/json",
        #   :example_key,
        #   [
        #     {
        #       id: "...", # insert example attributes
        #       name: "...", # insert example attributes
        #     },
        #   ],
        #   "Single <%= @object_name.singularize.camelcase %>",
        #   "Longer description of the example ..." # Example description, optional
        # # can define multiple examples, selectable in the UI

        # Automatic example generation
        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json" => {
              examples: {
                test_example: {
                  value: JSON.parse(response.body, symbolize_names: true),
                },
              },
            },
          }
          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end
        run_test!
      end

      response(404, "not found") do # rubocop: disable RSpec/NestedGroups
        # schema defined in swagger_helper.rb
        schema type: "object",
          properties: {
            message: { type: :string },
          }

        let(:id) { 999999999 }

        example "application/json",
          :example_key,
          { "error": "<%= @object_name.singularize %> not found, check the id." },
          "Single <%= @object_name.singularize.camelcase %>"

        run_test!
      end
    end
  end
}
    end
  end
end