require 'bundler'
require 'colorize'

class Pims::RswagInstaller
  def add_gems
    @gemfile_path = "./Gemfile"

    puts "\nUpdating gems(0/2)...\n".white
    gemlist = ['gem "rswag-ui", "~> 2.10.1"', 'gem "rswag-api", "~> 2.10.1"']
    gemlist_development_test = ['gem "rswag-specs", "~> 2.10.1"', 'gem "rspec-rails", "~> 5.1.2"']

    # read file into array of lines
    file_data = File.open(@gemfile_path).readlines.map(&:chomp)      

    # delete current gem mentions
    gems = ["rswag-api", "rswag-ui", "rswag-specs", "rspec-rails"]
    gems.each do |gem|
      index = file_data.index{|s| s.include?(gem)}
      
      if index != nil
        puts "Removed #{file_data[index].lstrip}".red
        file_data.delete_at(index)
      end
    end

    # write gems
    gemlist.each do |gem|
      puts "Added #{gem}".green
      file_data.append(gem)
    end

    gemlist_development_test.each do |gem|
      puts "Added #{gem}".green
      index = file_data.index{|s| s.include?("group :development, :test do")}
      if index.nil?
        file_data.append("group :development, :test do\n  #{gem}\nend")
      else
        file_data.insert(index + 1, "  " + gem)
      end
    end

    File.write(@gemfile_path,file_data.join("\n"))
  end

  def run_bundle
    # run bundle install
    puts "\nRun bundle install(1/2)...\n".white
    system('bundle install')
  end

  def run_generators
    puts "\nRun generator(2/2)...".white
    `rails g rswag:api:install; rails g rswag:ui:install; RAILS_ENV=test rails g rswag:specs:install; rails g rspec:install`
  end

  def set_default_host
    puts "Setting default swagger host -> localhost:3000"
    swagger_helper_path = "./spec/swagger_helper.rb"

    # read file into array of lines
    file_data = File.open(swagger_helper_path).readlines.map(&:chomp) if File.file?(swagger_helper_path)

    # locate default host setting
    index = file_data.index{|s| s.include?("www.example.com")}
    if index.nil?
      puts "Failed to find default host declaration in spec/swagger_helper.rb"
    else
      file_data[index] = "default: 'localhost:3000'"
    end

  end

  # def bundler_add_gems
  #   puts "\nrun bundle install\n"
  #   %x(bundle install)

  #   bundle_list = `bundle list --name-only`
  #   puts bundle_list

  #   gems = ["rswag-api", "rswag-ui", "rswag-specs", "rspec-rails"]
  #   gems.each do |gem|
  #     if bundle_list.include?(gem)
  #       puts "\ngem " + gem + " detected in Gemfile\n"
  #       system('bundle remove ' + gem)
  #     end
  #   end
  #   puts "removed old gems "
  #   system('bundle add rswag-api --skip-install -v "~>2.10.1"')
  #   system('bundle add rswag-ui --skip-install -v "~>2.10.1"')
  #   system('bundle add rswag-specs --skip-install -v "~>2.10.1" -group "development, test"')
  #   system('bundle add rspec/rails --skip-install -v "~>5.1.2" -group "development, test"')

  #   puts "Gemfile updated successfuly"
  # end
end