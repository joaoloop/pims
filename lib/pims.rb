class Pims
  def self.setup
    rswag_installer = RswagInstaller.new
    rswag_installer.add_gems
    rswag_installer.run_bundle
    rswag_installer.run_generators
    rswag_installer.set_default_host
    puts "\nSetup complete!"
  end

  def self.scan(api_folder)
    controller_scanner = ControllerScanner.new
    controller_scanner.parse(api_folder)
    controller_scanner.get_files    
    files_hash = controller_scanner.query_remove_files
    
    # handles all files removed
    if files_hash.nil? || files_hash.empty?
      puts "No files found in this folder!"
      return
    end

    answer = controller_scanner.ask_queue
    if answer
      files_hash.values.each do |file|
        puts file.yellow
        generate_spec(file)
      end
    end
  end

  def self.generate_spec(file)
    spec_generator = SpecGenerator.new(file)
    spec_generator.create_spec_file
    spec_generator.set_manifest
    spec_generator.build
    spec_generator.save
  end
end

require "pims/controller_scanner"
require "pims/rswag_installer"
require "pims/spec_generator"