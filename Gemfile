# frozen_string_literal: true

source "https://rubygems.org"

ruby "3.1.4"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# GENERAL #
gem "aasm", "~> 5.2"
gem "avo", "~> 2.8"
gem "amazing_print", "~> 1.3"
gem "axlsx_rails", "~> 0.6.1"
gem "bootsnap", "~> 1.7", ">= 1.7.3", require: false
gem "devise", "~> 4.8", ">= 4.8.1"
gem "devise-i18n", "~> 1.10", ">= 1.10.2"
gem "devise_invitable", "~> 2.0", ">= 2.0.6"
gem "faraday_middleware", "~> 1.2"
gem "faker", "~> 2.20"
gem "image_processing", "~> 1.12", ">= 1.12.1"
gem "jbuilder", "~> 2.11", ">= 2.11.5"
gem "money-rails", "~> 1.15"
gem "mqtt", "~> 0.5.0"
gem "pagy", "~> 5.10"
gem "paper_trail"
gem "parallel", "~> 1.23"
gem "pg_search", "~> 2.3", ">= 2.3.6"
gem "pg", "~> 1.2", ">= 1.2.3"
gem "prawn", "~> 2.4"
gem "puma", "~> 5.0"
gem "pundit", "~> 2.2"
gem "rack-cors", "~> 1.1", ">= 1.1.1"
gem "rails_semantic_logger", "~> 4.6"
gem "rails", "~> 6.1", ">= 6.1.3.1"
gem "rqrcode", "~> 2.1", ">= 2.1.1"
gem "sidekiq", "~> 6.2", ">= 6.2.2"
gem "sidekiq_alive", "~> 2.1"
gem "sidekiq-scheduler", "~> 3.1"
gem "sidekiq-status", "~> 2.1"
gem "tailwindcss-rails", "~> 2.0", ">= 2.0.8"
gem "turbo-rails", "~> 1.3", ">= 1.3.2"
gem "tzinfo-data", "~> 1.2021", ">= 1.2021.1"

# ASSETS #
gem "webpacker", "~> 5.2", ">= 5.2.1"

# S3 Service Adapter for Active Storage
gem "aws-sdk-s3", require: false

group :production do
  gem "sentry-delayed_job", "~> 4.7.2"
  gem "sentry-rails", "~> 4.7.2"
  gem "sentry-resque", "~> 4.7.2"
  gem "sentry-ruby", "~> 4.7.2"
  gem "sentry-sidekiq", "~> 4.7.2"
end

group :development, :test do
  gem "rspec-rails", "~> 5.1.2"
  gem "rswag-specs", "~> 2.10.1"
  gem "byebug", "~> 11.1", ">= 11.1.3"
  gem "rspec", "~> 3.10"
  gem "rails-controller-testing", "~> 1.0"
  gem "factory_bot_rails", "~> 6.2"
end

group :development do
  gem "annotate", "~> 3.2"
  gem "brakeman", "~> 5.0", require: false
  gem "bullet", "~> 6.1", ">= 6.1.4"
  gem "bundle-audit", "~> 0.1.0"
  gem "listen", "~> 3.5", ">= 3.5.1"
  gem "rubocop", "~> 1.12", ">= 1.12.1", require: false
  gem "rubocop-rails", "~> 2.9", ">= 2.9.1"
  gem "rubocop-performance", "~> 1.11"
  gem "rubocop-rspec", "~> 2.4"
  gem "rubocop-shopify", "~> 2.1"
  gem "solargraph", "~> 0.40.4"
  gem "spring", "~> 2.1", ">= 2.1.1"
  gem "web-console", "~> 4.1" # QUESTIONABLE
end

# Use Redis for Action Cable
gem "redis", "~> 4.0"


gem 'htmlbeautifier'
gem "rswag-ui", "~> 2.10.1"
gem "rswag-api", "~> 2.10.1"